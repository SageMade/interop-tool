﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace InteropGenerator.Properties
{
    public partial class SettingsDialogue : ThemedForm
    {
        string mySelectedTheme;
        private bool isDirty;
        ThemeEditor dlgThemeEditor;

        public SettingsDialogue()
        {
            InitializeComponent();
            txtDeclSpec.TextChanged += (a, b) => { isDirty = true; };
            txtNativePrefix.TextChanged += (a, b) => { isDirty = true; };
            txtIncludes.TextChanged += (a, b) => { isDirty = true; };
            dgvTypeMap.CellValueChanged += (a, b) => { isDirty = true; };
            nudTabSize.ValueChanged += (a, b) => { isDirty = true; };

            mySelectedTheme = "Default";
            cmbThemes.SelectedItem = mySelectedTheme;

            dlgThemeEditor = new ThemeEditor();

            if (Settings.Default.Includes != null)
            {
                string[] includes = new string[Settings.Default.Includes.Count];
                Settings.Default.Includes.CopyTo(includes, 0);
                txtIncludes.Lines = includes;
            }

            if (Settings.Default.TypeMap != null)
            {
                string[] typeMap = new string[Settings.Default.TypeMap.Count];
                Settings.Default.TypeMap.CopyTo(typeMap, 0);
                foreach(string line in typeMap) {
                    string[] vals = line.Split(',');
                    dgvTypeMap.Rows.Add(vals[0], vals[1]);
                }
            }

            nudTabSize.Value = Settings.Default.TabSize;

            //Adds additional #include directives to C++
            isDirty = false;
            
            PropegateBaseTheme(this);

            btnSave.FlatAppearance.BorderColor = Theme.Active.Colors["Border"];
            btnCancel.FlatAppearance.BorderColor = Theme.Active.Colors["Border"];
        }
        
        protected override void OnClosing(CancelEventArgs e) {
            if (isDirty) {
                DialogResult result = MessageBox.Show("Would you like to save these changes?", "You have unsaved changes", MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                    __SaveChanges();
                else if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
            base.OnClosing(e);
        }

        private bool __IsValidTypeName(object value)
        {
            foreach (char c in value.ToString())
                if (c != ' ' && !char.IsLetterOrDigit(c) && c != '_' && c != '-')
                    return false;
            return true;
        }

        private bool __ValidateRow(DataGridViewRow row)
        {
            return __IsValidTypeName(row.Cells[0].Value) && __IsValidTypeName(row.Cells[1].Value);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvTypeMap.Rows) {
                if (!row.IsNewRow) {
                    if (!__ValidateRow(row)) {
                        ThemedMessageBox.Show("Error parsing type map", "Type names are alphanumeric only", MessageBoxButtons.OK);
                        return;
                    }
                }
            }

            __SaveChanges();

            isDirty = false;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void __SaveChanges()
        {
            Settings.Default.TabSize = (int)nudTabSize.Value;

            Settings.Default.DeclSpec = txtDeclSpec.Text;
            Settings.Default.NativePrefix = txtNativePrefix.Text;
            if (Settings.Default.Includes == null)
                Settings.Default.Includes = new StringCollection();
            else
                Settings.Default.Includes.Clear();
            Settings.Default.Includes.AddRange(txtIncludes.Lines);


            if (Settings.Default.Includes == null)
                Settings.Default.TypeMap = new StringCollection();
            else
                Settings.Default.TypeMap.Clear();

            foreach (DataGridViewRow row in dgvTypeMap.Rows)
                if (!row.IsNewRow) 
                    Settings.Default.TypeMap.Add(string.Format("{0},{1}", row.Cells[0].Value, row.Cells[1].Value));

            Settings.Default.Save();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }

        private void btnThemeEdit_Click(object sender, EventArgs e) {
            if (dlgThemeEditor.ShowDialog() == DialogResult.OK) {
            }
        }

        private void cmbThemes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbThemes.SelectedIndex == cmbThemes.Items.Count - 1)
            {
                string name = "";
                if (TextModal.Show("Enter name for theme", "Name:", out name) == DialogResult.OK)
                {
                    cmbThemes.Items.Insert(cmbThemes.Items.Count - 1, name);
                    cmbThemes.SelectedItem = name;
                } else {
                    cmbThemes.SelectedItem = mySelectedTheme;
                }
            } else
            {
                mySelectedTheme = cmbThemes.SelectedItem as string;
            }
        }
    }
}
