﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    public class ButtonThemer : IThemer<Button>
    {
        protected override void ApplyTheme(Button control, Theme theme)
        {
            control.BackColor = theme.Colors["Button.Back", "Back"];
            control.ForeColor = theme.Colors["Button.Fore", "Fore"];
            control.FlatAppearance.BorderColor = theme.Colors["Button.Border", "Border"];
            control.FlatStyle = FlatStyle.Flat;
        }
    }
}
