﻿using InteropGenerator.Properties;
using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace InteropGenerator
{
    public static class TextBoxExtensions {
        private const int EM_SETTABSTOPS = 0x00CB;

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr h, int msg, int wParam, int[] lParam);

        public static void SetTabStopWidth(this TextBoxBase textbox, int width) {
            SendMessage(textbox.Handle, EM_SETTABSTOPS, 1, new int[] { width });
        }

        public static void BindTabSize(this TextBoxBase textbox, INotifyPropertyChanged notifier, string propName)
        {
            PropertyInfo pInfo = notifier.GetType().GetProperty(propName);
            if (pInfo != null && pInfo.PropertyType == typeof(int)) {

                int val = (int)pInfo.GetValue(notifier);
                textbox.SetTabStopWidth(val);

                notifier.PropertyChanged += (sender, prop) => {
                    if (prop.PropertyName == propName) {
                        val = (int)pInfo.GetValue(notifier);
                        textbox.SetTabStopWidth(val);
                    }
                };
            }
        }
    }
}
