﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InteropGenerator
{
    class ThemedNumericUpDown : UserControl
    {
        decimal __Value;
        public decimal Value
        {
            get { return __Value; }
            set { __Value = value < Minimum ? Minimum : value > Maximum ? Maximum : value; OnValueChanged(); Invalidate(); }
        }
        int __DecimalPlaces;
        public int DecimalPlaces
        {
            get { return __DecimalPlaces; }
            set { __DecimalPlaces = value; Invalidate(); }
        }

        decimal __Minimum;
        public decimal Minimum
        {
            get { return __Minimum; }
            set { __Minimum = value; Invalidate(); }
        }
        decimal __Maximum;
        public decimal Maximum
        {
            get { return __Maximum; }
            set { __Maximum = value; Invalidate(); }
        }

        public event EventHandler ValueChanged;

        public ThemedNumericUpDown() {
            Maximum = 100;
            SetStyle(ControlStyles.UserPaint | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
        }
        protected virtual void OnValueChanged()
        {
            ValueChanged?.Invoke(this, EventArgs.Empty);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Native.WM_NCPAINT) // WM_NCPAINT
            {
                IntPtr hDC = Native.GetWindowDC(m.HWnd);
                using (Graphics g = Graphics.FromHdc(hDC)) {
                    OnPaint(new PaintEventArgs(g, new Rectangle(0, 0, Width, Height)));
                }
                Native.ReleaseDC(m.HWnd, hDC);
            }
            base.WndProc(ref m);
        }

        protected void DrawArrow(Graphics g, Pen p, Rectangle bounds, float angle)
        {
            float scaleX = bounds.Width / 2.75f;
            Point center = new Point((bounds.Left + bounds.Right) / 2, (bounds.Top + bounds.Bottom) / 2);
            Matrix store = g.Transform;
            g.TranslateTransform(center.X, center.Y);
            g.RotateTransform(angle);
            g.TranslateTransform(0, scaleX / 2.0f);
            g.DrawLine(p, -scaleX, 0, 0, -scaleX);
            g.DrawLine(p, +scaleX, 0, 0, -scaleX);
            g.Transform = store;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle localBounds = new Rectangle(0, 0, Width - 1, Height - 1);
            e.Graphics.FillRectangle(Theme.Active.Brush("Back"), localBounds);
            //e.Graphics.DrawRectangle(Theme.Active.Pen("Fore"), localBounds);

            Rectangle up = new Rectangle(Width - 2- Height / 2, 0, Height / 2, Height / 2);
            Rectangle down = new Rectangle(Width - 2 - Height / 2, Height / 2, Height / 2, Height / 2);

            DrawArrow(e.Graphics, Theme.Active.Pen("Fore"), up, 0);
            DrawArrow(e.Graphics, Theme.Active.Pen("Fore"), down, -180.0f);

            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Near;
            format.LineAlignment = StringAlignment.Center;
            e.Graphics.DrawString(string.Format("{0:N" + DecimalPlaces + "}", Value), Font, Theme.Active.Brush("Fore"), localBounds, format);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Rectangle up = new Rectangle(Width - 2 - Height / 2, 0, Height / 2, Height / 2);
            Rectangle down = new Rectangle(Width - 2 - Height / 2, Height / 2, Height / 2, Height / 2);

            if (up.Contains(e.X, e.Y))
                Value += 1;
            else if (down.Contains(e.X, e.Y))
                Value -= 1;
        }

    }
}
